export var bikeImages = [
    {
        images: [
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/6_1000x.jpg?v=1555799304',selected:true},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/1_1000x.jpg?v=1555799304',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/5_1000x.jpg?v=1555799304',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/3_1000x.jpg?v=1555799304',selected:false},
        ],
        color:'azul_naranja',
        selected:true
    },
    {
        images: [
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/2_bc8e2bfe-df4b-407b-b111-55c9300d5db6_1000x.jpg?v=1555799308',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/1_48855bc7-fd5c-43a0-b2cc-e13606d6c9b3_1000x.jpg?v=1555799308',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/MG_8167_1000x.jpg?v=1555799308',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/MG_8164_1000x.jpg?v=1555799308',selected:false},
        ],
        color:'blanco',
        selected:false
    },
    {
        images: [
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/ornn_1000x.jpg?v=1566998781',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/MG_0794_1000x.jpg?v=1566998781',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/MG_0790_1000x.jpg?v=1566998781',selected:false},
            // {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/MG_0792_1000x.jpg?v=1566998781',selected:false},
        ],
        color:'morado',
        selected:false
    },
    {
        images: [
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/Bicicleta-Optimus-Tucana-2018-color-azul-bicicleta-todo-terreno-mtb-bicicleta-de-montana-6_1000x.jpg?v=1555799306',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/Bicicleta-Optimus-Tucana-2018-color-azul-bicicleta-todo-terreno-mtb-bicicleta-de-montana_1000x.jpg?v=1555799306',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/Bicicleta-Optimus-Tucana-2018-color-azul-bicicleta-todo-terreno-mtb-bicicleta-de-montana2_1000x.jpg?v=1555799306',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/Bicicleta-Optimus-Tucana-2018-color-azul-bicicleta-todo-terreno-mtb-bicicleta-de-montana-3_1000x.jpg?v=1555799306',selected:false},
        ],
        color:'azul',
        selected:false
    },
    {
        images: [
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/MG_0801_1000x.jpg?v=1555799313',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/MG_0818_1000x.jpg?v=1555799313',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/MG_0811_1000x.jpg?v=1555799313',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/MG_0803_1000x.jpg?v=1555799313',selected:false},
        ],
        color:'verde',
        selected:false
    },
    {
        images: [
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/01_1000x.jpg?v=1555799310',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/03_1000x.jpg?v=1555799310',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/04_1000x.jpg?v=1555799310',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/02_1000x.jpg?v=1555799310',selected:false},
        ],
        color:'naranja',
        selected:false
    },
    {
        images: [
            {url:'https://registrooptimus.com/imagenes/Tucana-negro-roja/red.jpg',selected:false},
            {url:'https://registrooptimus.com/imagenes/Tucana-negro-roja/marco.jpg',selected:false},
            {url:'https://registrooptimus.com/imagenes/Tucana-negro-roja/biela.jpg',selected:false},
            {url:'https://registrooptimus.com/imagenes/Tucana-negro-roja/pacha.jpg',selected:false},
        ],
        color:'rojo',
        selected:false
    },
    {
        images: [
            {url:'https://registrooptimus.com/imagenes/Tucana-negro-amarilla/yellow.jpg',selected:false},
            {url:'https://registrooptimus.com/imagenes/Tucana-negro-amarilla/marco.jpg',selected:false},
            {url:'https://registrooptimus.com/imagenes/Tucana-negro-amarilla/biela-amarilla.jpg',selected:false},
            {url:'https://registrooptimus.com/imagenes/Tucana-negro-amarilla/pacha.jpg',selected:false},
        ],
        color:'amarillo',
        selected:false
    }

]