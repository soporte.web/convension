export var bikeImages = [
    {
        images: [
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco_mtb-crater-amarillo---vista-1_1000x.png?v=1566080464',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco-mtb-crater-amarillo---vista-4_1000x.png?v=1566080464',selected:true},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco_mtb-crater-amarillo---vista-2_1000x.png?v=1566080464',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco_mtb-crater-amarillo---vista-3_1000x.png?v=1566080464',selected:false},
        ],
        color:'amarillo',
        selected:true
    },
    {
        images: [
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco-mtb-crater-negro-rojo-vista2_1000x.png?v=1566080464',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco-mtb-crater-negro-rojo-vista3_1000x.png?v=1566080464',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco-mtb-crater-negro-rojo-vista1_1000x.png?v=1566080464',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco-mtb-crater-negro-rojo-vista4_1000x.png?v=1566080464',selected:false},
        ],
        color:'rojo',
        selected:false
    },
    {
        images: [
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco-mtb-crater-negro-vista-1_1000x.png?v=1566080464',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco-mtb-crater-negro-vista-2_1000x.png?v=1566080464',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco-mtb-crater-negro-vista-4_1000x.png?v=1566080464',selected:false},
            {url:'https://cdn.shopify.com/s/files/1/2568/3124/products/casco-mtb-crater-negro-vista-3_1000x.png?v=1566080464',selected:false},
        ],
        color:'negro',
        selected:false
    }
]